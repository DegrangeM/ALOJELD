#import "@preview/cetz:0.2.0": canvas, plot, draw
#import "template.typ": *
#import "@preview/gentle-clues:0.4.0": *

#show: project.with(
  title: [ALOJELD - Règles],
  authors: (
    "Mathieu Degrange",
  ),
)

#align(center)[
_Un jeu réalisé par Mathieu Degrange et disponible sous licence CC-BY 4.0. 
_ #image("CCBY.svg", height: 0.75cm)
]

*À l'origine j'étais le directeur* (ALOJELD) est un jeu de société permettant de travailler les notions de coefficient directeur et d'ordonnée à l'origine d'une fonction affine. Il peut être joué de 2 à 4 joueurs.

= Matériel


- 81 cartes "Fonction affine" uniques, chacune contenant la courbe représentative d'une fonction affine. Le coefficient directeur ainsi que l'ordonnée à l'origine prennent pour valeur toutes les combinaisons de couples d'entiers entre - 4 et 4.

/* #scale(50%,box(height:5.74cm,width:5cm, stroke: 1pt + black, inset: 3pt, [
    #set align(center+horizon)
    #canvas(length: 1cm, {
          plot.plot(
            size: (3.5, 3.5),
            name: "plot",
            x-tick-step: 1,
            y-tick-step: 1,
            axis-style:"school-book",
            x-grid:true,
            y-grid:true,
            x-min:-5,
            x-max:5,
            x-label: $x$,
            x-format: x=>[#set text(6pt) ; $#x$],
            y-min:-5,
            y-max:5,
            y-label: $f(x)$,
            y-format: x=>[#set text(6pt) ; $#x$],
            {
              plot.add(
                style: (stroke: 1.5pt + red),
                domain: (-5, 5), x=> -2*x + 2
              )
            }
        )
    })
  ])) */

- 30 cartes “Objectif” uniques. Chaque carte est séparée en deux, la partie supérieure indique "le plus grand" ou "le plus petit" et la partie inférieure comporte un calcul.
- 8 cartes aides (4 cartes coefficient directeur et 4 cartes ordonnée à l'origine)
- Une carte "Directeur" / "Directrice".

= Mise en place

- Mélanger les cartes Objectif et former une pioche face cachée.
- Mélanger les cartes Fonction Affine et former une pioche face cachée.
- Chaque joueur pioche 2 cartes objectifs et 3 cartes Fonction Affine. // 4 au lieu de 3 ?
- Choisir un premier joueur et lui donner la carte "Directeur".

#pagebreak()

= Règles du jeu

Le jeu se déroule en plusieurs manches. Le but est de gagner le plus de manches.

#grid(columns: (1fr, 1fr), gutter: 5mm)[

== Déroulement d'une manche

Le joueur avec la _carte Directeur_ joue une _carte objectif_ qu’il pose au centre de la table, puis une _carte fonction affine_ qu’il pose devant lui.

Le joueur suivant (dans le sens des aiguilles d’une montre) joue une _carte fonction affine_.

On regarde s'il a joué une "meilleure carte" (voir ci-contre) que le joueur ayant actuellement la _carte Directeur_. Si c'est le cas, il récupère la _carte directeur_. En cas d'égalité, la _carte Directeur_ ne change pas de joueur.

On continue dans le sens des aiguilles d'une montre jusqu'à ce que chaque joueur ait joué une _carte fonction affine_. On regarde ensuite qui est la personne avec la _carte Directeur_. Celle-ci a gagné la manche et elle récupère la carte objectif qu'elle place face cachée devant elle (cela signifie qu'elle a gagné un point).

On défausse ensuite les _cartes fonctions affines_ jouées. Chaque joueur repioche afin d'avoir 2 _cartes objectifs_ et 3 _cartes fonction affines_ dans sa main.

On commence ensuite une nouvelle manche.

][

#set text(10pt)
  

#idea(title:"Calcul du score d'une carte")[Pour calculer le score d'une carte fonction affine, on regarde la carte objectif actuelle. Celle-ci indique une formule, il faut alors remplacer les variables $m$ et $p$ de cette formule par les valeurs du coefficient directeur $m$ et de l'ordonnée à l'origine $p$ de la carte fonction affine d'après sa représentation graphique.]

#idea(title:"Comparaison de deux cartes fonctions affines")[On calcule le score de la carte fonction affine du joueur ayant actuellement la carte Directeur.
On calcule ensuite le score de la carte fonction affine du joueur venant de jouer sa carte.
Si la carte objectif indique "le plus grand", c'est la carte fonction affine avec le plus grand score qui gagne.
Si la carte objectif indique "le plus petit", c'est la carte fonction affine avec le plus petit score qui gagne.
Si les deux cartes ont le même score, c'est la carte fonction affine du joueur ayant la carte directeur qui gagne.]

]

#info(title:"Exemple d'une manche")[#set text(10pt)
Pierre, Sophie, Julie et Sébastien jouent. Pierre a la carte directeur. Il pose la carte objectif "Le plus petit" $m + p$. Il pose ensuite une carte fonction affine dont le coefficient directeur est $m=-3$ et l'ordonnée à l'origine $p=-1$. Le score de sa carte est donc $m+p=(-3)+(-1)=-4$. Sophie joue une carte fonction affine avec $m=1$ et $p=-2$. Le score de sa carte est $m+p=1+(-2)=-1$. Sophie ne bat pas la carte de Pierre car il fallait obtenir le score le plus petit et $-4 < -1$. Julie quand à elle pose une carte fonction affine avec $m=-2$ et $p=-4$. Le score de sa carte est donc $m+p=(-2)+(-4)=-6$. Elle bat donc la carte de Pierre car $-6 < -4$ et récupère donc la carte directeur. Sébastien quand à lui joue une carte fonction affine avec $m=-3$ et $p=-3$. Le score de sa carte est donc $m+p=(-3)+(-3)=-6$. Il est a égalité avec Julie, elle conserve donc sa carte directeur. Tout le monde a joué, Julie a gagné la manche et récupère donc la carte objectif qu'elle place face cachée devant elle pour signifier quelle a gagné un point. Les cartes fonction affines jouées sont défaussées et tout le monde repioche afin d'avoir 2 cartes objectifs et 3 cartes fonctions affines dans sa main.]

== Fin du jeu

Le jeu se termine lorsque qu’il n’y a plus assez de cartes dans la pioche. Le gagnant est le joueur avec le plus de cartes objectifs face cachée devant lui. En cas d'égalité, il y a plusieurs gagnants.


