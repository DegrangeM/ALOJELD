#import "@preview/cetz:0.2.0": canvas, plot, draw
#import "@preview/gentle-clues:0.4.0": *

#set page(/*flipped:true, */margin: (left: 5mm, right: 5mm, top: 5mm, bottom: 5mm))
#set text(font: "Linux Libertine", lang: "fr")
#set par(justify: true)

// Pour la prochaine fois : créer un array contenant une liste d'objet avec un type associé et utiliser cet array pour générer automatiquement tous les dos de cartes


// CARTES FONCTIONS AFFINES

#let patAffine = pattern(size: (5pt, 5pt))[
  #place(square(fill:rgb("#50F05050")))
  #place(line(start: (0%, 0%), end: (100%, 100%), stroke:1pt + rgb("#00500050")))
  #place(line(start: (0%, 100%), end: (100%, 0%), stroke:1pt + rgb("#00500050")))
]

#let dosFonctionAffine = (box(outset:-1.5pt,fill: patAffine, place(image("dosFonctionAffine.svg")), height:100%,width:100%),)

#let fonctions = ()

#let mm = (-4,-3,-2,-1,0,1,2,3,4)
#let pp = (-4,-3,-2,-1,0,1,2,3,4)


#for m in mm { // Se limiter à -3;3 ?
  for p in pp {
    
    fonctions.push(canvas(length: 1cm, {
      plot.plot(size: (3.5, 3.5),
        x-tick-step: 1,
        y-tick-step: 1,
        axis-style:"school-book",
        x-grid:true,
        y-grid:true,
        x-min:-5,
        x-max:5,
        y-min:-5,
        y-max:5,
        y-label:$f(x)$,
        {
          plot.add(
            style: (stroke: 1.5pt + red),
            domain: (-5, 5), x=> m*x+p)
        })
    }))
  }
}

#let cartesFonctions = ()

// On ajoute les dos sauf pour la dernière page partielle qui sere gérée manuellement
#for k in range(0, fonctions.len(), step: 5*4) {
  let pageFonctions = fonctions.slice(k, calc.min(k + 5*4, fonctions.len()))
  if pageFonctions.len() == 5*4 {
      cartesFonctions = (..cartesFonctions,..pageFonctions,..dosFonctionAffine*5*4)
  } else {
      cartesFonctions = (..cartesFonctions,..pageFonctions)
  }
}

// CARTES OBJECTIFS

#let patObjectif = pattern(size: (5pt, 5pt))[
  #place(square(fill:rgb("#F0F0A050")))
  #place(line(start: (0%, 0%), end: (100%, 100%), stroke:1pt + rgb("#ffddcc")))
  #place(line(start: (0%, 100%), end: (100%, 0%), stroke:1pt + rgb("#cceeff")))
]

#let dosObjectif = (box(outset:-1.5pt, fill: patObjectif, place(image("dosObjectif.svg"))/*+ place(box(fill:white, stroke: 1pt+black, inset:5pt, radius:5pt, [*1 point*]),dx:100%-52pt,dy:100%-24pt)*/, height:100%,width:100%),)

#let objectifs = ($ m $, $ m $, $ p $, $ p $, $ m+p $, $ m+p $, $ m-p $, $ p-m $, $ m^2 $, $ p^2 $, $ m+2p $, $ 2m + p $, $ m^2 + p^2 $, $ abs(m-p)  $, $ abs(m) + abs(p) $)

#let carteObjectif(objective,content, objColor) = {
  set text(18pt)
  table(
    columns: (1fr),
    rows: (1fr, 1fr),
    stroke:3pt + black,
    fill: (col,row) => if row == 0 {objColor},
    objective,
    content
  )  
}

#let objectifsMax = objectifs.map(x=>carteObjectif([Le plus grand],x, color.hsl(20deg, 100%, 90%)))
#let objectifsMin = objectifs.map(x=>carteObjectif([Le plus petit],x, color.hsl(2000deg,100%,90%)))

#let cartesObjectif = (..objectifsMax, ..objectifsMin)

// CARTES AIDE

#let cartesAide = (
  box(
    fill:rgb("#ffcc0010"),
    inset: 10pt,
    [ 
      #set text(8pt)
      // Le *coefficient directeur* $m$ correspond au nombre d'unité vers laquelle on se déplace vers le haut lorsqu'on se déplace d'une unité vers la droite. Ici $m=2$.
      Le *coefficient directeur* $m$ correspond à la variation de $f(x)$ lorsque $x$ augmente de 1. Ici $m=-2$.

    ] +
    canvas(length: 1cm, {
        plot.plot(size: (3, 3),
          name: "plot",
          x-tick-step: 1,
          y-tick-step: 1,
          axis-style:"school-book",
          x-grid:true,
          y-grid:true,
          x-min:-4,
          x-max:4,
          x-label: none,
          x-format: x=>[#set text(6pt) ; $#x$],
          y-min:-4,
          y-max:3,
          y-label: none,
          y-format: x=>[#set text(6pt) ; $#x$],
          {
            plot.add-anchor("m1",(1,-1))
            plot.add-anchor("m2",(2,-1))
            plot.add-anchor("m3",(2,-3))
            plot.add(
              style: (stroke: 1.5pt + red),
              domain: (-5, 5), x=> -2*x + 1
            )
          })
          draw.line("plot.m1", "plot.m2", mark: (end: ">", length:3pt, fill:blue), stroke: blue)
          draw.line("plot.m2", "plot.m3", mark: (end: ">", length:3pt, fill:blue), stroke: blue, name: "line")
          // draw.content("line.mid", $ #h(3pt) m $, anchor:"west")
        })
  ),
  box(
      fill:rgb("#ffcc0010"),
      inset: 10pt,
      [ 
        #set text(8pt)
        L'*ordonnée à l'origine* $p$ correspond à l'ordonnée à laquelle la droite coupe l'axe des ordonnées. Ici $p=-3$.
      ] +
      canvas(length: 1cm, {
          plot.plot(size: (3, 3),
            name: "plot",
            x-tick-step: 1,
            y-tick-step: 1,
            axis-style:"school-book",
            x-grid:true,
            y-grid:true,
            x-min:-4,
            x-max:4,
            x-label: none,
            x-format: x=>[#set text(6pt) ; $#x$],
            y-min:-4,
            y-max:3,
            y-label:none,
            y-format: x=>[#set text(6pt) ; $#x$],
            {
              plot.add-anchor("p",(0,-3))
              plot.add(
                style: (stroke: 1.5pt + red),
                domain: (-5, 5), x=> 1.5*x - 3
              )
            })
            // ffdc00AA
            draw.circle("plot.p", radius:0.15, fill:color.rgb("#00dcffAA"), stroke: 1pt+ black)
        })
    )
  )*4

#let carteDirecteur = (box(place(image("boss.svg", width: 100%)), height:100%,width:100%),)
#let dosDirecteur = (box(place(image("dosBoss.svg", width: 100%)), height:100%,width:100%),)



#let patAide = pattern(size: (5pt, 5pt))[
  #place(square(fill:rgb("#ffcc0050")))
  #place(line(start: (0%, 0%), end: (100%, 100%), stroke:1pt + rgb("#ffcc0050")))
  #place(line(start: (0%, 100%), end: (100%, 0%), stroke:1pt + rgb("#ffcc0050")))
]

#let dosAide = (box(outset:-1.5pt,fill: patAide, place(image("dosAide.svg")), height:100%,width:100%),)

// CREATION DE LA GRILLE

#table(
  columns: ((21cm-1cm)/4,)*4,
  rows: ((29.7cm-1cm)/5,)*5,
  inset: 0pt,
  stroke: 3pt + black,
  align: center+horizon,
  ..cartesFonctions,
  ..cartesObjectif.slice(0,5*4-1),
  ..dosObjectif*(3),
  ..dosFonctionAffine,
  ..dosObjectif*(4*4),
  ..cartesObjectif.slice(5*4-1,count:11),
  ..cartesAide,
  ..carteDirecteur,
  ..dosObjectif*8,
  ..dosAide,
  ..dosObjectif*3,
  ..dosAide*4,
  ..dosDirecteur,
  ..dosAide*3,
)