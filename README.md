À l'origine j'étais le directeur (ALOJELD) est un jeu de société permettant de travailler les notions de coefficient directeur et d'ordonnée à l'origine d'une fonction affine. Il peut être joué de 2 à 4 joueurs.

Le but du jeu est de gagner le plus de manches. Les joueurs jouent des cartes `Fonction affine` afin de réussir à remporter la manche en fonction de la carte `Objectif` jouée par le `Directeur`.

![](/uploads/f32def868063df4f9819297ef57e0688/cartesnumeriques2_page-0112.jpg){width=164px} ![](/uploads/19a29955517b6a762393c408b836d89b/cartesnumeriques2_page-0033.jpg){width=164px} 

![](/uploads/5e202f5ac64f327bfb7641cba1898cc6/cartesnumeriques2_page-0113.jpg){width=164px}
![](/uploads/ef94c34bd8d2dd9437aa7b2fb6d8dc15/objectifs__6_.jpg){width=164px}
![](/uploads/052ff8659bd10007d7c405885ed4f2d6/objectifs__20_.jpg){width=164px}

Le fichier `main.pdf` contient les règles du jeu ainsi que le matériel à imprimer, découper et éventullement plastifier.

Le projet a été réalisé à l'aide du logiciel Typst (alternative à LaTeX). Les sources sont donc au format `.typ`.
